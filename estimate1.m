function [I,Q]=estimate1(fd, signal, PRN)
    IF = 4.348 * 1e6;
    I = ifft(fft((signal)*cos(2*pi*(IF + fd))) * fft(PRN));
    Q = ifft(fft((signal)*sin(2*pi*(IF + fd))) * fft(PRN));
end